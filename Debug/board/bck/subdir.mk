################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../board/bck/board.c \
../board/bck/clock_config.c \
../board/bck/pin_mux.c 

OBJS += \
./board/bck/board.o \
./board/bck/clock_config.o \
./board/bck/pin_mux.o 

C_DEPS += \
./board/bck/board.d \
./board/bck/clock_config.d \
./board/bck/pin_mux.d 


# Each subdirectory must supply rules for building sources it contributes
board/bck/%.o: ../board/bck/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -D"CPU_MKL46Z256VLH4" -I../CMSIS -I../board -I../drivers -I../startup -I../utilities -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


