#include "uart.h"
	
void uartInitialize() {
	CLOCK_EnableClock(kCLOCK_Uart0);

	SIM->SOPT2 = SIM_SOPT2_UART0SRC(2);
	
	UART0->C2 &= ~(UART_C2_RE_MASK | UART_C2_TE_MASK);
	
	// Baud Rate: 9600 =
	UART0->C4 = UART0_C4_OSR(15);
	
	UART0->BDH = UART_BDH_SBR(1>>8);
	UART0->BDL = UART_BDL_SBR(1);
	
	UART0->C1 = UART0->S2 = UART0->C3 = 0;
	
	/*NVIC_SetPriority(UART0_IRQn, 2);
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_EnableIRQ(UART0_IRQn);	
	__enable_irq();*/
	
	UART0->C2 = UART_C2_RE_MASK | UART_C2_TE_MASK;// | UART_C2_RIE_MASK;
	
}


/*void UART0_IRQHandler() {
	NVIC_ClearPendingIRQ(UART0_IRQn);
	data_in = UART0->D;

	if (number != 0x31) {
		LED_GREEN_ON();
	}
	else {
		LED_GREEN_OFF();
	}
	UART0->D = 0x33;

}*/
