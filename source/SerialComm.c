/*
 * SerialComm.c
 *
 *  Created on: 8 Eyl 2016
 *      Author: bilal
 */

#include "SerialComm.h"

comm_mode_t changeMode(uint8_t incCmd) {

	switch (incCmd) {
		case START_CMD:
			return CONFIG_MODE;

		case END_CMD:
			return IDLE;

		case START_SAMP:
			return SAMPLING_MODE;

		case END_SAMP:
			return TRANSFER_MODE;

		default:
			return ERR_MODE;
	}
}

comm_config_state_t changeConfigMode(uint8_t incCmd) {

	switch (incCmd) {
		case GET_MAX_CH_NUM:
		case GET_MAX_SAMP_RATE:
		case GET_MAX_BAUD_RATE:
		case GET_AVAIL_MEM_SIZE:
			return REPLY_CMD;

		case SET_SAMP_RATE:
		case SET_BAUD_RATE:
		case SET_SAMP_DURATION:
			return SET_CONFIG;

		default:
			return ERR_UNKNOWN_CONFIG;
	}

}

void replyCmd(serial_cmd_t cmd) {
	uint8_t replyBuffer[4];
	switch (cmd) {
		case GET_MAX_SAMP_RATE:
			replyBuffer[0] = 3;
			replyBuffer[1] = (uint8_t)(MAX_SAMPLING_FREQ >> 16);
			replyBuffer[2] = (uint8_t)(MAX_SAMPLING_FREQ >> 8);
			replyBuffer[3] = (uint8_t)(MAX_SAMPLING_FREQ);

			UART_WriteBlocking(UART0_COMPAT, replyBuffer, 3);
			break;

		case GET_MAX_CH_NUM:
			replyBuffer[0] = 1;
			replyBuffer[1] = MAX_CH_NUM;

			UART_WriteBlocking(UART0_COMPAT, replyBuffer, 3);
			break;

		case GET_MAX_BAUD_RATE:
			replyBuffer[0] = 3;
			replyBuffer[1] = (uint8_t)(MAX_BAUD_RATE >> 16);
			replyBuffer[2] = (uint8_t)(MAX_BAUD_RATE >> 8);
			replyBuffer[3] = (uint8_t)(MAX_BAUD_RATE);

			UART_WriteBlocking(UART0_COMPAT, replyBuffer, 3);
			break;

		case GET_AVAIL_MEM_SIZE:
			replyBuffer[0] = 2;
			replyBuffer[1] = (uint8_t)(AVAIL_MEM_SIZE >> 8);
			replyBuffer[2] = (uint8_t)(AVAIL_MEM_SIZE);
			replyBuffer[3] = (uint8_t)(AVAIL_MEM_SIZE);

			UART_WriteBlocking(UART0_COMPAT, replyBuffer, 3);
			break;
		default:
			break;
	}
}

config_status_t applyCmd(serial_cmd_t cmd) {
	uint8_t answerBuffer[3];
	UART_ReadBlocking(UART0_COMPAT, answerBuffer, 3);
	uint32_t freq;
	switch (cmd) {
		case SET_SAMP_RATE:
			freq = (((uint32_t)answerBuffer[0]) << 16)
							| (((uint32_t)answerBuffer[1]) << 8)
							| ((uint32_t)answerBuffer[2]);

			if (setSamplingFrequency(freq)) {return SUCCESS;}
			else {return INVALID_SAMP_FREQ;}
		case SET_BAUD_RATE:
			return SUCCESS;

		case SET_SAMP_DURATION:
			samplingDuration = (((uint32_t)answerBuffer[0]) << 16)
							| (((uint32_t)answerBuffer[1]) << 8)
							| ((uint32_t)answerBuffer[2]);

			return SUCCESS;
		default:
			return UNKNOWN_CMD;
	}
}


