#include "leds.h"
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_port.h"
#include "fsl_clock.h"
#include "fsl_uart.h"

#define UART0_IRQn (IRQn_Type) 12
#define UART0_COMPAT ((UART_Type *) UART0_BASE)
#define MAX_BAUD_RATE 500000

void uartInitialize(void);

