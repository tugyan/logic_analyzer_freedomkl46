#include "Sampling.h"

void initChannels(void) {
	gpio_pin_config_t ch_conf = {kGPIO_DigitalInput, 0};

	for (int ch = 0; ch < 8; ++ch) {
		FGPIO_PinInit(CH_GPIO, ch, &ch_conf);
	}

	pit_config_t pit_conf = {true};

	PIT_Init(PIT, &pit_conf);


	PIT_EnableInterrupts(PIT, SAMPLING_TIMER, kPIT_TimerInterruptEnable);
	EnableIRQ(PIT_IRQn);

	PIT_SetTimerPeriod(PIT, SAMPLING_TIMER, 0xFFFF);
}

void startSampling(void) {
	currentSample = 0;
	memset(chCounters, 0, sizeof(chCounters));
    memset(dataPointers, 0, sizeof(dataPointers));
    previousSamples = ((uint8_t) CH_GPIO->PDIR);
    isSampling = true;
    PIT_StartTimer(PIT, SAMPLING_TIMER);
}

bool setSamplingFrequency(uint32_t freq) {
	uint32_t bus_freq = CLOCK_GetBusClkFreq();
	if(freq >= bus_freq) {
		return false;
	}
	else {
		PIT_StopTimer(PIT, SAMPLING_TIMER);
		uint32_t ldv = round((((double)bus_freq)/((double)freq)) - 1);
		PIT_SetTimerPeriod(PIT, SAMPLING_TIMER,ldv);
	}
	samplingFreq = freq;
	return true;
}

void sampleChannels(void) {

	uint8_t currentSamples = ((uint8_t) CH_GPIO->PDIR);
	uint8_t isChToggled = previousSamples ^ currentSamples;
	for (int chNum = 0; chNum < active_ch_num; ++chNum) {
       if ((isChToggled >> chNum) & 0x1) {
		   if (chCounters[chNum] > BYTE_MAX) {

			   dataBuffer[chNum][dataPointers[chNum]] = 0;
			   ++dataPointers[chNum];

			   if (chCounters[chNum] > SHORT_MAX ) {
				   dataBuffer[chNum][dataPointers[chNum]] = ((uint8_t)(chCounters[chNum] >> 16));
				   ++dataPointers[chNum];
			   }

			   dataBuffer[chNum][dataPointers[chNum]] = ((uint8_t)(chCounters[chNum] >> 8));
			   ++dataPointers[chNum];
		   }

		   dataBuffer[chNum][dataPointers[chNum]] = ((uint8_t)chCounters[chNum]);
		   ++dataPointers[chNum];

		   chCounters[chNum] = 0;
       }
       else {
    	   ++chCounters[chNum];
       }
	}
	previousSamples = currentSamples;
}

void PIT_IRQHandler(void) {
    PIT_ClearStatusFlags(PIT, SAMPLING_TIMER, kPIT_TimerFlag);
	sampleChannels();
	++currentSample;

	if (currentSample == samplingDuration) {
		PIT_StopTimer(PIT, SAMPLING_TIMER);
		isSampling = false;
	}
}

