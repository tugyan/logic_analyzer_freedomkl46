#include "board.h"
#include <stdlib.h>
#include <math.h>
#include "fsl_pit.h"

#define BYTE_MAX 255
#define SHORT_MAX 65535
#define MAX_SAMPLING_FREQ 12000000
#define CH_GPIO FGPIOC
#define SAMPLING_TIMER kPIT_Chnl_0
#define MAX_CH_NUM 8
#define AVAIL_MEM_SIZE 3500

uint8_t active_ch_num = MAX_CH_NUM;

bool isSampling = false;
uint32_t currentSample = 0;
uint32_t samplingFreq = MAX_SAMPLING_FREQ;
uint8_t dataBuffer[MAX_CH_NUM][AVAIL_MEM_SIZE];
uint16_t dataPointers[MAX_CH_NUM] = {0};
uint8_t previousSamples = 0;
uint32_t samplingDuration = 10000;
uint32_t chCounters[MAX_CH_NUM] = {0};

void initChannels(void);
void startSampling(void);
bool setSamplingFrequency(uint32_t);
void sampleChannels(void);
