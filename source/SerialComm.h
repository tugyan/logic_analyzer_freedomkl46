#include "board.h"
#include "uart.h"
#include "Sampling.h"

enum _comm_mode{IDLE, CONFIG_MODE, SAMPLING_MODE, TRANSFER_MODE, ERR_MODE}
		typedef comm_mode_t;

enum _comm_config_state{REPLY_CMD, SET_CONFIG, ERR_UNKNOWN_CONFIG}
		typedef comm_config_state_t;

enum _config_status{SUCCESS = 0x0, INVALID_SAMP_FREQ = 0x1 ,UNKNOWN_CMD = 0xFF }
		typedef config_status_t;


enum _serial_cmd{   START_CMD = 0x0,
					GET_MAX_CH_NUM = 0x1,
					GET_MAX_SAMP_RATE = 0x2,
					GET_MAX_BAUD_RATE = 0x3,
					GET_AVAIL_MEM_SIZE = 0x5,
					SET_SAMP_RATE = 0x6,
					SET_BAUD_RATE = 0x7,
					SET_SAMP_DURATION = 0xC,
					SEND_ERR = 0x7E,
					END_CMD = 0x7F,
					START_SAMP = 0xFE,
					END_SAMP = 0xFF} typedef serial_cmd_t;


const uint8_t err_msg[3] = {'E', 'r', 'r'};

comm_mode_t changeMode(uint8_t);
comm_config_state_t changeConfigMode(uint8_t);
void replyCmd(serial_cmd_t);
config_status_t applyCmd(serial_cmd_t);

